from xml.etree import ElementTree

# 1. Parsing the xml document
with open('reference-rss20.xml', 'rt') as f:
    tree = ElementTree.parse(f)

# print(tree)

# 2. Traversing the tree
for node in tree.findall('.//channel/item/title'):
    print(node.text)
