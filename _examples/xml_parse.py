import http.client
import xml.etree.ElementTree as ET


url = 'https://netzpolitik.org/feed'

conn = http.client.HTTPSConnection("netzpolitik.org")
conn.request("GET", "/feed/")
feed = conn.getresponse()
xml = feed.read().decode('utf-8')

with open('netzpolitik-org.xml', 'w') as file:
    file.write(xml)

tree = ET.fromstring(xml)
items = tree.findall('channel/item')

title = items[0].find('title')
link = items[0].find('link')
pubdate = items[0].find('pubDate')
print(title.text)
print(link.text)
print(pubdate.text)
