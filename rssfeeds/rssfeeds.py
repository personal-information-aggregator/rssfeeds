#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

import sys
import os
import logging
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import ParseError

from connectivity import Url
from connectivity import Connection

logging.basicConfig(filename='rssfeeds.log', level=logging.DEBUG)

class XMLElement:

    def __init__(self, element: Element) -> None:
        self.el = element

    def child_element(self, name: str) -> str:
        el = self.el.find(name)
        if el is None:
            return 'No ' + name + ' found!'
        else:
            return str(el.text)


class RSSFeed:

    def __init__(self, xml: str) -> None:
        self.xml = xml
        try:
            self.tree = ET.fromstring(self.xml)
        except ParseError as e:
            print(e)

    def title(self) -> str:
        channel = XMLElement(self.tree.find('.//channel'))
        title = channel.child_element('title')
        line = ''.join(['=' for e in range(len(title))])
        return str(line + '\n' + title.upper() + '\n' + line + '\r\n')

    def content(self) -> list:
        items = [XMLElement(el) for el in self.tree.findall('.//channel/item')]
        contents = []
        for item in items[:5]:
            title = item.child_element('title')
            link = item.child_element('link')
            contents.append(str(title + '\n' + link + '\r\n'))
        return contents


class InputFile:

    def __init__(self, filename: str) -> None:
        self.filename = filename

    def urls(self) -> list:
        try:
            with open(self.filename, 'r') as f:
                return [Url(line) for line in f]
        except FileNotFoundError as e:
            print(e)


if __name__ == '__main__':
    feeds_input = InputFile('feeds.txt')
    connections = [Connection(url) for url in feeds_input.urls()]
    feeds = [RSSFeed(connection.data) for connection in connections]
    for feed in feeds:
        print(feed.title())
        for content in feed.content():
            print(content)
