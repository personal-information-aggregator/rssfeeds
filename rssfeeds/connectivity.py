#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

import logging
from urllib.request import urlopen
from urllib.parse import urlparse
from urllib.error import HTTPError
from urllib.error import URLError
from http import HTTPStatus

logging.basicConfig(filename='connectivity.log', level=logging.DEBUG)


class Url:

    def __init__(self, url: str) -> None:
        self.url = url.rstrip()

    def __repr__(self) -> str:
        return str(self.url)

    def netloc(self) -> str:
        return str(urlparse(self.url).netloc)

    def path(self) -> str:
        return str(urlparse(self.url).path)

    def string(self) -> str:
        return str(self.url)


class Connection:

    def __init__(self, url: Url) -> None:
        self.url = url
        self._data = str()

        print('connect to: ' + self.url.netloc())
        logging.info('connect to: ' + self.url.string())

        try:
            self.response = urlopen(self.url.string())
        except HTTPError as e:
            logging.error(e)
        except URLError as e:
            logging.error(e)
        else:
            if (self.response.getcode() == HTTPStatus.OK):
                self._data = self.response.read().decode('utf-8')
                logging.info(self.response.getcode())
            else:
                self._data = 'No XML'

    @property
    def data(self) -> str:
        return str(self._data)
