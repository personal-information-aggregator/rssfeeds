#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

import unittest

from rssfeeds import InputFile
from rssfeeds import RSSFeed


class TestXMLElement(unittest.TestCase):

    pass


class TestInputFile(unittest.TestCase):

    def test_correct_filename(self):
        i = InputFile('feeds.txt')
        self.assertIsInstance(i.urls(), list)

    def test_wrong_filename(self):
        i = InputFile('feed.txt')
        self.assertIs(i.urls(), False)

class TestRSSFeed(unittest.TestCase):

    def setUp(self):
        xmlfile = open('_examples/reference-rss20.xml', 'r')
        xmlstr = ''.join(xmlfile.readlines())
        xmlfile.close()
        feed = RSSFeed(xmlstr)

    def test_feed_title(self):
        pass

    def test_article_url(self):
        pass

    def test_article_title(self):
        pass


if __name__ == '__main__':
    unittest.main()
