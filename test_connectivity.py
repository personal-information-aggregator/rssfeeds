#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

import unittest

from connectivity import Url
from connectivity import Connection


class TestUrl(unittest.TestCase):

    def setUp(self):
        self.url = Url('https://www.heise.de/feeds')

    def test_netloc(self):
        self.assertEqual('www.heise.de', self.url.netloc())

    def test_path(self):
        self.assertEqual('/feeds', self.url.path())

    def test_str(self):
        self.assertIs(str, type(self.url.string()))


class TestConnection(unittest.TestCase):

    def setUp(self):
        heise = Url('https://www.heise.de/')
        self.conn = Connection(heise)

    def test_if_title_is_in_response(self):
        self.assertIn('heise online - IT-News, Nachrichten und Hintergründe',
                      self.conn.data)

    def test_if_response_is_string(self):
        self.assertIs(type(self.conn.data), str)


if __name__ == '__main__':
    unittest.main()
